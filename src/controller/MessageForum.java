package controller;

import model.Forum;
import model.User;
import model.Message;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;

/**
 * Servlet implementation class Forums
 */
@WebServlet(urlPatterns = {"/messageforum"})
public class MessageForum extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageForum() {
        super();
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        HttpSession session = request.getSession();
        int id;
if(request.getParameter("forumId")!=null) {
    id = Integer.parseInt((String) request.getParameter("forumId"));
     session.setAttribute("ForumId",id);
}

    id= (int) session.getAttribute("ForumId");

        //System.out.println("forum id to display: " + id);
        List<Forum> forumList =  (List<Forum>)session.getAttribute("all_forums");
        Forum forum;
        if (forumList == null) {
            forum = new Forum(id);
        } else {
            forum = findForumToBeSelected(forumList, id);
            if (forum == null ) {
                System.err.println("PROBLEM, forum with id " + id + " not present in list " + forumList.toString());
            }
        }

        if(request.getParameter("delete")!=null){
            Integer idToDelete= Integer.valueOf(request.getParameter("delete"));
            forum.loadAllMessages();
            // this way we don't request again on the database
            for (Message message : forum.getMessages()) {
                if(message.get_id()==idToDelete) {
                    message.delete();
                }
            }
        }







        if(request.getParameter("content")!=null){

            String content = toHTML(request.getParameter("content"));
            User user = (User)session.getAttribute("connected_user");


        Message m=new Message(content,user,forum);
            m.insert();




        }




        forum.loadAllMessages();
        session.setAttribute("forum", forum);
        RequestDispatcher rd = request.getRequestDispatcher("/messageforum.jsp");
        rd.forward(request, response);
    }


    //toDO deplacer dans forum
    private Forum findForumToBeSelected(List<Forum> forumList, int id) {
        for (Forum forum : forumList) {
            if (forum.get_id() == id){
                return forum;
            }
        }
        return null;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public String toHTML(String str) {
        String out = "";
        for (char c: str.toCharArray()) {
            if(!Character.isLetterOrDigit(c))
                out += String.format("&#x%x;", (int)c);
            else
                out += String.format("%s", c);

        }
        return out;
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }
}
