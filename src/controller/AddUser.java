package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Forum;
import model.User;


/**
 * Servlet implementation class Subscriptions
 */

@WebServlet(name = "AddUser", urlPatterns = {"/adduser"})
public class AddUser extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUser() {
        super();
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("connected_user");
        if (user == null) {
            System.out.println("Attribut connected_user not set");
            //TODO  rediriger
        }
        try {
            user.loadSubscription();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        session.setAttribute("subscriptions", user.getForumSubscriptions());
        redirect("/adduser.jsp", request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        User user = (User)session.getAttribute("connected_user");

        int forum_id = Integer.parseInt(request.getParameter("unsub"));
        try {
            Forum f = new Forum(forum_id);
            user.deleteForumSubscriptions(f);
            user.loadSubscription();
            session.setAttribute("subscriptions", user.getForumSubscriptions());
            redirect("/hub.jsp", request,response);


        }catch(Exception e) {
            e.printStackTrace();
        }


    }

}
