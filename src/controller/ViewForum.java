package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;


import model.Forum;
import model.User;


/**
 * Servlet implementation class Forums
 */
@WebServlet(urlPatterns = {"/ViewForum"})
public class ViewForum extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewForum() {
        super();
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("connected_user");

        try {
            List<Forum> forums = Forum.findAll();
            List<Forum> forumsNotAlreadyFollow=Forum.findNotAlreadyFollow(user.get_id());
            session.setAttribute("all_forumsNotfollow", forumsNotAlreadyFollow);
            boolean more_to_sub;

            if(user == null){
                System.out.println("User null redirect to login");
                response.sendRedirect("/index.html");
            }
            user.loadSubscription();
            if(forums!=null && user.getForumSubscriptions() != null) {
                 more_to_sub = !(user.getForumSubscriptions().size() == forums.size());
            }else{
                more_to_sub=true;
            }

            session.setAttribute("all_forums", forums);
            session.setAttribute("more", more_to_sub);
            response.setContentType("text/html; charset=UTF-8");
            RequestDispatcher rd = request.getRequestDispatcher("/viewforum.jsp");
            rd.forward(request, response);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        try {
            int forum_id = Integer.parseInt(request.getParameter("sub"));
            Forum f = new Forum(forum_id);
            HttpSession session = request.getSession();
            User u = (User)session.getAttribute("connected_user");
            u.addForumSubscription(f);
            redirect("/hub.jsp", request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }
}
