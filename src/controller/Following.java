package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;


import model.Forum;
import model.User;


/**
 * Servlet implementation class Forums
 */
@WebServlet(urlPatterns = {"/following"})
public class Following extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Following() {
        super();
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("connected_user");
        if (user != null) {
        try {

            if (request.getParameter("sub") != null) {
                Integer forum_id = Integer.parseInt(request.getParameter("sub"));
                if (forum_id != null) {
                    Forum f = new Forum(forum_id);
                    user.addForumSubscription(f);
                    redirect("/ViewForum", request, response);
                    return;
                }
            }


            if (request.getParameter("unfollow") != null) {
                Integer forum_id = Integer.parseInt(request.getParameter("unfollow"));
                try {
                    System.out.println("unfollow bien obtenu");
                    Forum f2 = new Forum(forum_id);
                    User u = (User) session.getAttribute("connected_user");
                    u.deleteForumSubscriptions(f2);
                    u.loadSubscription();
                    session.setAttribute("subscriptions", u.getForumSubscriptions());
                    redirect("/hub.jsp", request, response);
                    return;

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                user.loadSubscription();
                session.setAttribute("subscriptions", user.getForumSubscriptions());
                response.setContentType("text/html; charset=UTF-8");
            }

            } catch (Exception e) {
            e.printStackTrace();
        }

            RequestDispatcher rd = request.getRequestDispatcher("/following.jsp");
            rd.forward(request, response);
        }else {
            System.out.println("user is null" );
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub




    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }
}
