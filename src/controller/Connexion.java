/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import  model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anael
 */
@WebServlet(name = "Connexion", urlPatterns = {"/Connexion"})
public class Connexion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (request.getAttribute("disconnect") != null) {
                boolean disconnected = (Boolean) request.getAttribute("disconnect");
                if (disconnected == true) {
                    redirect("/connexion/connexion_disconnect.jsp", request, response);
                    return;
                }
            }
            // Vérifier si le login existe
            User user = User.FindByloginAndPwd(request.getParameter("username"), request.getParameter("password"));
            if (user == null) {
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet Connexion</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Echec :mot de passe ou login érroné </h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
            } else {
                HttpSession session = request.getSession();
                session.setAttribute("connected_user", user);
                session.setAttribute("login", user.getLogin());
                String role = "";
                if (user.getRole() != null) {
                    role = user.getRole().toString();
                }
                session.setAttribute("role", role);
                response.setContentType("text/html;charset=UTF-8");
                if ("admin".equalsIgnoreCase(role)) {
                    try (PrintWriter out = response.getWriter()) {
                        out.println("<!DOCTYPE html>");
                        out.println("<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\" type=\"text/css\" />");
                        out.println("<html><head><title>Navigation Administrateur</title></head>");
                        out.println("<body>");
                        out.println("<h1>Hello " + session.getAttribute("login") + "</h1>");
                        out.println("<nav> <ul>");
                        out.println(" <li>Connected</li>");
                        out.println("<li><a href='NouveauUtilisateur.html'>Créer un nouveau utilisateur</a></li>");
                        out.println(" <li><a href='UserManager'>Afficher la liste des utilisateurs</a></li>");
                        out.println(" <li><a href='Deconnexion'>Déconnecter</a></li>");
                        out.println("</ul>");
                        out.println("</nav>");
                        out.println("</body>");
                        out.println("</html>");
                        redirect("/Hub", request, response);
                        return;
                    }

                } else {
                    redirect("/Hub", request, response);
                    return;

                }

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, ex.getStackTrace().toString(), ex);
        }
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.

    }

}
