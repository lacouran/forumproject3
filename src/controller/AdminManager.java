package controller;

import model.Forum;
import model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;
import controller.MessageForum;
import static model.Forum.findForumToBeSelected;
import model.Message;
import model.Forum;

/**
 * Servlet implementation class Forums
 */
@WebServlet(urlPatterns = {"/AdminManager"})
public class AdminManager extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManager() {
        super();
    }

    private void redirect(String jsp_path, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(jsp_path).forward(request, response);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        HttpSession session = request.getSession();
        if(request.getParameter("deleteUser")!=null){
            User UserTodelete=User.findByLogin(request.getParameter("usertoDelete"));
            User userConnected = (User)session.getAttribute("connected_user");

            if(UserTodelete!=userConnected) {
                UserTodelete.ActionBeforeDelete(UserTodelete);
                UserTodelete.delete();
                redirect("/hub.jsp", request, response);
                return;
            }
            else{
                System.out.println("Try to self delete");
            }

        }




        if (request.getParameter("delete_forum") != null) {
            try {
                Integer delete = Integer.parseInt(request.getParameter("delete_forum"));
                List<Forum> forumList = (List<Forum>) session.getAttribute("all_forums");
                Forum forum;
                if (forumList == null) {
                    System.out.println("there is no forum in the database");
                } else {
                    forum = findForumToBeSelected(forumList, delete);
                    if (forum == null) {
                        System.out.println(" forum with id " + delete + "does not exist  " + forumList.toString());
                    } else {

                        forum.loadAllMessages();
                        if (forum.getMessages() != null) {
                            for (Message message : forum.getMessages()) {
                                System.out.println("message id" + message.get_id());
                                message.delete();
                            }
                        }
                        forum.deleteForumSubscription();
                        forumList.remove(forum);
                        session.setAttribute("all_forums", forumList);
                        System.out.println("forum id" + forum.get_id());
                        forum.delete();
                    }
                }
                redirect("/hub.jsp", request, response);
                return;

            } catch (Exception e) {
                e.printStackTrace();
            }

                }else{

            System.out.println("no deletion forums");
        }
        if(request.getParameter("validator")!=null){
            System.out.println("post request received");
            String title = request.getParameter("title");
            String owner = request.getParameter("owner");
            String desc = request.getParameter("desc");
            System.out.println(title+owner+desc);
            if(title==null|| owner==null||desc==null){
                System.out.println("smtg went wrong, values not entered");
                redirect("/hub.jsp", request, response);

            }else{

                    User user=User.findByLogin(owner);
                            Forum f = new Forum(title,desc,user);
                            f.insert();
                            redirect("/hub.jsp", request, response);
                            return;

            }


        }else{

        System.out.println("request post not found");
        }

    }



    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }
}
