package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.List;

public class User extends ActiveRecord {

    /**
     * Attributs
     */
    private String lastName;
    private String firstName;
    private String login;
    private Gender gender;
    private String Gender2;
    private String password;
    private Role role = Role.Other;
    private static String QUERY = "SELECT * from users";
    private List<Forum> forumSubscriptions;

    public enum Role {
        Other, Admin;
    }

    public enum Gender {
        M, F, Oth
    }

    public User() {
        _builtFromDB = false;
    }

    public User(String ln, String fn, String lg, Gender gdr, String pwd) {
        this.lastName = ln;
        this.firstName = fn;
        this.login = lg;
        this.gender = gdr;
        this.password = pwd;
        _builtFromDB = false;
    }
    public User(String ln, String fn, String lg, String gdr, String pwd) {
        this.lastName = ln;
        this.firstName = fn;
        this.login = lg;
        this.Gender2 = gdr;
        this.password = pwd;
        _builtFromDB = false;
    }


    public User(ResultSet result) throws SQLException {
        this._id = result.getInt("id");
        this.firstName = result.getString(2);
        this.lastName = result.getString(3);
        this.login = result.getString(4);
        this.password = result.getString(7);
        this.gender = Gender.valueOf(result.getString("gender"));
        this.role = Role.values()[result.getBoolean("is_admin") ? 1 : 0];
        _builtFromDB = true;
    }

    public User(int id) throws IOException, ClassNotFoundException, SQLException {
      build(Integer.toString(id));
    }

    @Override
    protected String _getByID(String id) {
        return  "select * from `db_sr03`.`users` where `id` = '" + id + "';";
    }

    @Override
    protected void construireModel(ResultSet res) throws SQLException, IOException, ClassNotFoundException {
        this._id = res.getInt("id");
        this.firstName = res.getString(2);
        this.lastName = res.getString(3);
        this.login = res.getString(4);
        this.gender = Gender.valueOf(res.getString("gender"));
        this.role = Role.values()[res.getBoolean("is_admin") ? 1 : 0];
        this.password = res.getString(7);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    // active record
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.lastName);
        hash = 97 * hash + Objects.hashCode(this.firstName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.lastName, other.getLastName())) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.getFirstName())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User [Last Name: " + lastName + ", First Name: " + firstName + ", Login: " + login + ", Gender: "
                + gender + ", Password: " + password + ", Role: " + role + "]";
    }

    @Override
    protected String _update() {
        return "UPDATE `db_sr03`.`users` SET `fname` = '" + firstName + "', `lname` = '" + lastName + "',"
                + " `login` = '" + login + "', `is_admin` = '" + (role == Role.Admin ? "1" : "0") + "', `gender` = '"
                + gender + "', `pwd` = '" + password + "' WHERE `id` = " + _id + ";";
    }

    @Override
    protected String _insert() {
        return "INSERT INTO `db_sr03`.`users` (`fname`, `lname`, `login`, `gender`, `is_admin`,`pwd`) " + "VALUES ('"
                + firstName + "', '" + lastName + "', '" + login + "', '" + gender + "', '"
                + (role == Role.Admin ? "1" : "0") + "','" + password + "');";
    }

    @Override
    protected String _delete() {
        return "DELETE FROM `db_sr03`.`users` WHERE (`id` = '" + _id + "');";
    }


    protected void DeleteSub(int id)throws IOException, ClassNotFoundException, SQLException{
        Connection conn = BDDConector.getInstance();
        String select_query = "DELETE FROM `db_sr03`.`subscription` WHERE (`forum_id` = ? AND `user_id` =?);";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, Integer.toString(id));
        sql.setString(2, Integer.toString(this._id));
        sql.executeUpdate();
    }

    protected void addSub(int id)throws IOException, ClassNotFoundException, SQLException{
        Connection conn = BDDConector.getInstance();
        System.out.println("id forum"+id+"id user"+this._id);
        String select_query = "insert  into  `db_sr03`.`subscription` values (?,?);";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, Integer.toString(id));
        sql.setString(2, Integer.toString(this._id));
        sql.executeUpdate();
    }



    public static User FindByloginAndPwd(String login, String pwd)
            throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "SELECT * from `db_sr03`.`users` where `login` = ? and `pwd` = ? ;";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, login);
        sql.setString(2, pwd);
        ResultSet res = sql.executeQuery();
        if (res.next()) {
            User u = new User(res);
            return u;

        }
        return null;
    }




    public static User findByLastAndFirstName(String fname, String lname)
            throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "select * from `db_sr03`.`users` where `fname` = ? and `lname` = ? ;";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, fname);
        sql.setString(2, lname);
        ResultSet res = sql.executeQuery();
        if (res.next()) {
            User u = new User(res);
            return u;

        }
        return null;
    }
    public static User findByLogin(String login)
            throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "select * from `db_sr03`.`users` where `login` = ?  ;";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, login);
        ResultSet res = sql.executeQuery();
        if (res.next()) {
            User u = new User(res);
            return u;
        }
        return null;
    }

    public void loadSubscription()
            throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "select * FROM `forums` \n" +
                "INNER JOIN `subscription` on `subscription`.`forum_id` = `forums`.`id` \n" +
                "where `subscription`.`user_id` = ?";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, String.valueOf(this._id));
        ResultSet res = sql.executeQuery();
        List<Forum> result = new ArrayList<>();
        while (res.next()) {
            result.add(new Forum(res));
        }
        this.forumSubscriptions = result;
    }

    public User findById(int id) throws IOException, ClassNotFoundException, SQLException {
        return new User(id);
    }

    public static List<User> findAll() throws IOException, ClassNotFoundException, SQLException {
        List<User> listUser = new ArrayList<User>();

        Connection conn = BDDConector.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(QUERY);
        while (res.next()) {
            User newUser = new User(res);
            listUser.add(newUser);
        }

        return listUser;
    }

    public List<Forum> getForumSubscriptions() {
        return forumSubscriptions;
    }

    public void setForumSubscriptions(List<Forum> forumSubscriptions) {
        this.forumSubscriptions = forumSubscriptions;
    }

    public boolean addForumSubscription(Forum f) throws ClassNotFoundException, SQLException, IOException {
        this.addSub(f.get_id());
        return forumSubscriptions.add(f);
    }

    public boolean deleteForumSubscriptions(Forum f) throws SQLException, IOException, ClassNotFoundException {
        this.DeleteSub(f.get_id());
        return forumSubscriptions.remove(f);
    }
    public boolean isSubscribedTo(Forum f) {
        return forumSubscriptions.contains(f);
    }

    public void ActionBeforeDelete(User u) throws SQLException, IOException, ClassNotFoundException {

        Connection conn = BDDConector.getInstance();
        String select_query = "delete  FROM `subscription` \n"+
                "where `subscription`.`user_id` = ?";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, String.valueOf(u._id));
        sql.executeUpdate();

        String select_query2 = "update `messages`  \n"+
                "set  `messages`.`id_user` =-1+\n" +
                "            `id_user` = ?";
        PreparedStatement sql2 = null;
        sql2 = conn.prepareStatement(select_query);
        sql2.setString(1, String.valueOf(u._id));
        sql2.executeUpdate();



    }



}


