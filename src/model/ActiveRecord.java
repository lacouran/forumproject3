/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lounis
 */
public abstract class ActiveRecord {

    protected int _id;

    protected boolean _builtFromDB;

    protected abstract String _update();

    protected abstract String _insert();

    protected abstract String _delete();

    protected abstract String _getByID(String id);

    protected abstract void construireModel(ResultSet res) throws SQLException, IOException, ClassNotFoundException;

    public void build(String id)  throws SQLException, IOException, ClassNotFoundException {
        Connection conn = BDDConector.getInstance();
        Statement sql = null;
        sql = conn.createStatement();
        ResultSet res = sql.executeQuery(_getByID(id));
        if (res.next()) {
            construireModel(res);
            _builtFromDB = true;
        }
    }

    public void save() throws SQLException, IOException, ClassNotFoundException {
        Connection conn = BDDConector.getInstance();
        Statement s = conn.createStatement();
        if (_builtFromDB) {
            System.out.print("Executing this command : " + _update() + "\n");
            s.executeUpdate(_update());
        } else {
            System.out.println("Executing this command: " + _insert() + "\n");

            // Récuoérer la valeur de clé artificielle (auto_incrément)
            s.executeLargeUpdate(_insert(), Statement.RETURN_GENERATED_KEYS);
            _builtFromDB = true;
            ResultSet r = s.getGeneratedKeys();
            while (r.next()) {
                _id = r.getInt(1);
            }
        }
       
    }

    public void delete() throws IOException, ClassNotFoundException, SQLException {

        Connection conn = BDDConector.getInstance();
        Statement s = conn.createStatement();

            System.out.println("Executing this command: " + _delete() + "\n");
            s.executeUpdate(_delete());
           

    }

    public void insert() throws IOException, ClassNotFoundException, SQLException {

        Connection conn = BDDConector.getInstance();
        Statement s = conn.createStatement();
        System.out.println("Executing this command: " + _insert() + "\n");
        s.executeUpdate(_insert());
    }


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }


    public void remove() throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        Statement s = conn.createStatement();
        if (_builtFromDB) {
            System.out.println("Executing this command: " + "\n");
            this.delete();

        } else {
            System.out.println("Objet non persistant!");
        }
    }



}
