package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Message extends ActiveRecord {
    private String content;
    private Date datePublished;
    private User publisher;
    private Forum destination;
    private static String DATE_FORMAT = "yyyy-mm-dd HH:mm:ss";

    public Message() {
        _builtFromDB = false;
        this.datePublished = new Date();
    }

    public Message(String content, User publisher, Forum destination) {
        this.content = content;
        this.publisher = publisher;
        this.datePublished = new Date();
        this.destination = destination;
    }

    @Override
    protected String _getByID(String id) {
        return "SELECT * from messages where `id` = '" + id + "';";
    }

    @Override
    protected void construireModel(ResultSet res) throws SQLException, IOException, ClassNotFoundException {
        this._id = res.getInt("id");
        this.content = res.getString(2);
        this.publisher = new User(res.getInt(3));
        this.destination = new Forum(res.getInt(4));
        try {
            this.datePublished = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(res.getString(5));
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Message(ResultSet resultSet) throws SQLException, IOException, ClassNotFoundException {
        construireModel(resultSet);
    }
    private Message(int id) throws SQLException, IOException, ClassNotFoundException {
       build(Integer.toString(id));
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date d) {
        this.datePublished = d;
    }

    public User getPublisher() {
        return this.publisher;
    }

    @Override
    public String toString() {
        return "Message [Content: " + content + ", Publisher: " + publisher + ", Destination: " + destination + "]";
    }

    public void setPublisher(User pub) {
        this.publisher = pub;
    }

    @Override
    protected String _delete() {
        return "DELETE FROM `db_sr03`.`messages` WHERE (`id` = '" + _id + "');";
    }

    @Override
    protected String _insert() {
        return "INSERT INTO messages (`content`, `id_user`, `id_forum`) " + "VALUES ('" + content + "', '"
                + publisher.get_id() + "', '" + destination.get_id() + "');";



    }

    @Override
    protected String _update() {
        return "update`db_sr03`.`messages` set  `content`='" + content + "', " + "`id_user`='" + publisher.get_id()
                + "', `id_forum`='" + destination.get_id() + "'  WHERE (`id` = '" + _id + "');";
    }

    public static Message findById(int id) throws IOException, ClassNotFoundException, SQLException {
        return new Message(id);
    }

    public static List<Message> findByForum(int idForum) throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "SELECT * FROM messages WHERE id_forum = '" + idForum + "';";
        Statement sql = null;
        sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);
        List<Message> result = new ArrayList<Message>();
        while (res.next()) {
            result.add(new Message(res));
        }
        return !result.isEmpty() ? result : null;
    }

    public static List<Message> findbyUser(int idUser) throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        String select_query = "SELECT * FROM messages WHERE id_user = '" + idUser + "';";
        Statement sql = null;
        sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);

        List<Message> result = new ArrayList<Message>();

        while (res.next()) {
            result.add(new Message(res.getInt("id")));
        }
        return !result.isEmpty() ? result : null;
    }

    public Forum getDestination() {
        return destination;
    }

    public void setDestination(Forum f) {
        destination = f;
    }
}
