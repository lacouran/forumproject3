package model;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class Forum extends ActiveRecord {

    private String title;
    private String description;
    private List<Message> messages = new ArrayList<Message>();
    private User owner;
    private static String QUERY = "SELECT * from forums";

    public Forum(String titre, String description, User user) {
        this.title = titre;
        this.description = description;
        this.owner = user;
    }

    @Override
    protected String _getByID(String id) {
        return "select * from `db_sr03`.`forums` where `id` = '" + id + "';";
    }


    @Override
    protected void construireModel(ResultSet res) throws SQLException, IOException, ClassNotFoundException {
        this._id = res.getInt("id");
        this.title = res.getString(2);
        this.owner = new User(res.getInt(3));
        this.description = res.getString(4);
    }

    public Forum(ResultSet res) throws SQLException, IOException, ClassNotFoundException {
        construireModel(res);
    }

    public Forum(int id) throws SQLException, IOException, ClassNotFoundException {
        build(Integer.toString(id));

    }

    // DB access method
    @Override
    protected String _delete() {
        return "DELETE FROM `db_sr03`.`forums` WHERE (`id` = '" + _id + "');";
    }

    @Override
    protected String _insert() {
        return "INSERT INTO `db_sr03`.`forums` (`title`, `owner`,`description`) " + "VALUES ('" + title + "', '"
                + owner.get_id() + "','"+description+"');";
    }

    @Override
    protected String _update() {
        return "UPDATE `db_sr03`.`forums` SET `title` = '" + title + "', " + "`owner`='" + owner.get_id()
                + "', `description` = '" + description + "'   WHERE (`id` = '" + _id + "');";
    }

    @Override
    public String toString() {
        return "Forum [Title: " + title + ", Description: " + description + ", Owner: " + owner + "]";
    }




    public void loadAllMessages() throws IOException, ClassNotFoundException, SQLException {
        List<Message> messages = Message.findByForum(_id);
        this.messages = messages;
    }

    public void addMessage(Message m) throws IOException, ClassNotFoundException, SQLException {
        m.save();
    }

    public static List<Forum> findAll() throws IOException, ClassNotFoundException, SQLException {
        Connection conn = BDDConector.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(QUERY);
        List<Forum> result = new ArrayList<Forum>();
        while (res.next()) {
            result.add(new Forum(res));
        }
        return !result.isEmpty() ? result : null;
    }
    public static List<Forum> findNotAlreadyFollow(int userid) throws IOException, ClassNotFoundException, SQLException {
        String id=Integer.toString(userid);
        Connection conn = BDDConector.getInstance();
        //Statement sql = conn.createStatement();

        String query="select * from `forums`,`subscription`where`forums`.`id` " +
                "not in (select `subscription`.`forum_id` from `forums`,`subscription` where `subscription`.`user_id`=?) and `subscription`.`user_id`=?;";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(query);
        sql.setString(1, String.valueOf(id));
        sql.setString(2, String.valueOf(id));
        ResultSet res = sql.executeQuery();
        List<Forum> result = new ArrayList<Forum>();
        while (res.next()) {
            result.add(new Forum(res));
        }
        return !result.isEmpty() ? result : null;
    }




    public static Forum findForumToBeSelected(List<Forum> forumList, int id) {
        for (Forum forum : forumList) {
            if (forum.get_id() == id){
                return forum;
            }
        }
        return null;
    }



    public void deleteForumSubscription()
            throws IOException, ClassNotFoundException, SQLException {

        Connection conn = BDDConector.getInstance();
        String select_query = "delete  FROM `subscription` where `subscription`.`forum_id` = ?";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, String.valueOf(this._id));
        sql.executeUpdate();
        System.out.println("destruction subscription");
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Forum forum = (Forum) o;
        return Objects.equals(title, forum.title) &&
                Objects.equals(description, forum.description) &&
                Objects.equals(messages, forum.messages) &&
                Objects.equals(owner, forum.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, messages, owner);
    }

    public Forum findById(int id) throws IOException, ClassNotFoundException, SQLException {
        return new Forum(id);
    }

    public List<Message> getMessages() {
        return this.messages;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User o) {
        this.owner = o;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String t) {
        this.title = t;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

}
