-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 31 Mai 2020 à 18:21
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_sr03`
--

-- --------------------------------------------------------

--
-- Structure de la table `forums`
--

CREATE TABLE `forums` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `owner` int(11) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `forums`
--

INSERT INTO `forums` (`id`, `title`, `owner`, `description`) VALUES
(16, 'camus', 4, 'maman est morte'),
(17, 'facebook', 22, 'oui'),
(18, 'trombone', 22, 'musique'),
(19, 'GE37', 4, 'non');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_forum` int(11) NOT NULL,
  `date_published` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`id`, `content`, `id_user`, `id_forum`, `date_published`) VALUES
(9, 'Salut cest un message', -1, 1, '2020-08-07 21:47:01'),
(11, 'bjr', 22, 18, '2020-05-27 13:04:47'),
(12, 'genre', 22, 18, '2020-05-27 13:04:52'),
(13, 'salut', 22, 18, '2020-05-27 13:56:42'),
(14, 'ntm&#x27;', 22, 18, '2020-05-27 13:56:48'),
(15, 'yes', 22, 18, '2020-05-27 13:56:51'),
(17, 'Zuck&#x20;essaye&#x20;de&#x20;nous&#x20;dominer&#x2c;&#x20;c&#x20;un&#x20;reptilien&#x20;&#x20;faite&#x20;gaffe&#x20;eveillez&#x20;vous&#x20;lol', -1, 17, '2020-05-27 15:11:46'),
(18, 'oui', 22, 17, '2020-05-27 15:55:47'),
(19, 'Faut&#x20;clairement&#x20;faire&#x20;gaffe&#x20;il&#x20;est&#x20;vilain', 22, 17, '2020-05-27 15:56:04'),
(20, 'JÃ&#xa9;peur&#x20;', 19, 17, '2020-05-28 17:47:28');

-- --------------------------------------------------------

--
-- Structure de la table `subscription`
--

CREATE TABLE `subscription` (
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `subscription`
--

INSERT INTO `subscription` (`forum_id`, `user_id`) VALUES
(17, 19),
(16, 22);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `gender` enum('M','F','Oth','') NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `login`, `gender`, `is_admin`, `pwd`) VALUES
(-1, 'anonyme', 'anonyme', 'onsenfiche', 'M', 0, 'abc'),
(19, 'anael', 'lacour', 'lacouran', 'M', 0, 'abc'),
(22, 'florian', 'lacour', 'uro', 'M', 1, 'abc');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`forum_id`,`user_id`),
  ADD KEY `fk_users` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `forums`
--
ALTER TABLE `forums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `fk_forum` FOREIGN KEY (`forum_id`) REFERENCES `forums` (`id`),
  ADD CONSTRAINT `fk_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`forum_id`) REFERENCES `forums` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
