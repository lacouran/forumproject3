<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="model.Forum"%>
<%@ page import="java.util.Set"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Subscriptions</title>
    <link rel="stylesheet" type="text/css" media="all" href="./mystyle.css" />
</head>
<body>
<h1>My Subscriptions</h1>

<%
    Set<Forum> forums = (Set<Forum>) request.getSession().getAttribute("subscriptions");
    if (!forums.isEmpty()){
        out.println("<div class = 'fieldset'>");
        out.println("	<div class = 'fieldset_label'>");
        out.println("		<span> Subscriptions <span>");
        out.println("	</div>");
        out.println("<table>");
        out.println("<tr>");
        out.println("	<th> Title </th>");
        out.println("</tr>");

        for (Forum f : forums) {

            String title = f.getTitle();
            int id = f.get_id();
            out.println("<div class = 'field'>");
            out.println("<tr>");
            out.println("	<td>" + title + "</td>");
            out.println("	<td><form action='SingleForum' method='POST'><input type='hidden' name='view' value=" + id
                    + ">" + "<button>View</button></form></td> <td><form action='Subscriptions' method='POST'><input type='hidden' name='unsub' value=" + id
                    + ">" + "<button>Unsubscribe</button></form></td>");

            out.println("</tr>");
            out.println("</div>");
        }
        out.println("</table>");
        out.println(" <a href='hub.jsp'>Home</a>");
        out.println("</div>");
    }else {
        out.println("You haven't subscribed to any forum yet");
        out.println(" <br> <a href='hub.jsp'>Home</a>");
    }

%>

</body>
</html>