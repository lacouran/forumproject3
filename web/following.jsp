<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="model.Forum"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Subscriptions</title>
    <link rel="stylesheet" type="text/css" media="all" href="./mystyle.css" />
</head>
<body>
<h1>My Subscriptions</h1>
<%
    List<Forum> forums = (List<Forum>) request.getSession().getAttribute("subscriptions");
    if (!forums.isEmpty()){
        out.println("<div class = 'fieldset'>");
        out.println("	<div class = 'fieldset_label'>");
        out.println("		<span> Subscriptions <span>");
        out.println("	</div>");
        out.println("<table>");
        out.println("<tr>");
        out.println("	<th> Title </th>");
        out.println("	<th> Link </th>");
        out.println("	<th> Unfollow </th>");
        out.println("</tr>");

        for (Forum f : forums) {

            String title = f.getTitle();
            int id = f.get_id();
            out.println("<div class = 'field'>");
            out.println("<tr>");
            out.println("	<td>" + title + "</td>");
            out.println("	<td> <a href='messageforum?forumId=" + f.get_id() +"'>see</a></td>");
            out.println("	<td> <a href='following?unfollow=" +f.get_id() +"'>unfollow</a></td>");
            out.println("</tr>");
            out.println("</div>");
        }
        out.println("</table>");
        out.println(" <a href='Hub'>Home</a>");
        out.println("</div>");
    }else {
        out.println("You haven't subscribed to any forum yet");
        out.println(" <br> <a href='Hub'>Home</a>");
    }

%>

</body>
</html>