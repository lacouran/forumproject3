<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="model.Forum"%>
<%@ page import="java.util.List"%>
<%@ page import="model.Message" %>
<%@ page import="model.User" %>
<html>
<head>
    <title>View Forum ${forum.getTitle()}</title>
</head>
<body>

<%
    Forum forum = (Forum) request.getSession().getAttribute("forum");
    User user = (User)session.getAttribute("connected_user");
    if (forum == null || forum.getMessages() == null) {

        out.println("No message on this forum");
    } else {
        out.println("<div class = 'fieldset'>");
        out.println("	<div class = 'fieldset_label'>");
        out.println("		<span> Forum " + forum.getTitle() + " <span>");
        out.println("	</div>");


        out.println("<table>");
        out.println("<tr>");
        out.println("	<th> Author </th>");
        out.println("	<th> content </th>");
        out.println("	<th> date </th>");

        String role= (String) session.getAttribute("role");
        if ("admin".equalsIgnoreCase(role)){
            out.println("	<th> Supprimer </th>");
        }
        out.println("</tr>");

        for (Message message : forum.getMessages()) {

            out.println("<div class = 'field'>");
            out.println("<tr>");
            if(message.getPublisher()==null){
                out.println("	<td>Unknown user</td>");
            }else {
                out.println("	<td>" + message.getPublisher().getFirstName() + " " + message.getPublisher().getLastName() + "</td>");
            }
            out.println("	<td>" + message.getContent() + "</td>");
            out.println("	<td>" + message.getDatePublished() + "</td>");
            if ("admin".equalsIgnoreCase(role)){
                out.println("	<td> <a href='messageforum?delete=" +message.get_id() +"'>delete</a></td>");
            }
            out.println("</tr>");
            out.println("</div>");
        }





    }


    out.println(" <a href='Hub'>Home</a>");
%>

<div class='fieldset'>
    <div class='fieldset_label'>
        <span>Post message</span>
    </div>
    <form method='POST' action='messageforum?forumId=${forum.get_id()}'>
        <div class='field>'>
            <label>Content : <input type='text' name='content'
                                    placeholder='Type your message ' required></label>
        </div>
        <input type='hidden' name='action' value='post_message'> <input
            type='hidden' name='forum_id' value="${forum.get_id()}">
        <button>Post</button>
    </form>
</div>



</body>
</html>
