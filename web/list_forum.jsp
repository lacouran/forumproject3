<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="model.Forum"%>
<%@ page import="model.User"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Forums</title>
    <link rel="stylesheet" type="text/css" media="all" href="./mystyle.css" />
</head>
<h1>All Forums</h1>
<body>
<%
    List<Forum> forums = (List<Forum>) request.getSession().getAttribute("all_forums");

    if (forums!=null) {
        User u = (User) request.getSession().getAttribute("connected_user");

        if ((boolean) request.getSession().getAttribute("more")) {
            out.println("<div class = 'fieldset'>");
            out.println("	<div class = 'fieldset_label'>");
            out.println("		<span> Forums <span>");
            out.println("	</div>");
            out.println("<table>");
            out.println("<tr>");
            out.println("	<th> Title </th>");
            out.println("</tr>");

            for (Forum f : forums) {
                if (!u.isSubscribedTo(f)) {
                    String title = f.getTitle();
                    int id = f.get_id();
                    out.println("<div class = 'field'>");
                    out.println("<tr>");
                    out.println("	<td>" + title + "</td>");
                    out.println("	<td><form action='Forums' method='POST'><input type='hidden' name='sub' value="
                            + id + ">" + "<button>Subscribe</button></form></td>");

                    out.println("</tr>");
                    out.println("</div>");
                }

            }
            out.println("</table>");
            out.println(" <a href='hub.jsp'>Home</a>");
            out.println("</div>");
        } else {
            out.println("You have already subscribed to all forums !");
            out.println(" <br> <a href='hub.jsp'>Home</a>");
        }
    }else{
        out.println("bug");
    }

%>


</body>
</html>