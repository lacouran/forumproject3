<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
    <title>hub</title>
</head>
<body>
<header>
    <form method="POST" action="Logout">
        <input type="hidden" name="action" value="Logout">
        <button class="btn-logout form-btn">Logout</button>
    </form>
    <h1>hub</h1>
</header>


<div class="fieldset">
    <div class="fieldset_label">
        <span>Personal information</span>
    </div>
    <div class="field">
        <label>Last Name : </label><span>

        ${connected_user.getLastName()} </span>
    </div>
    <div class="field">
        <label>First Name : </label><span>	${connected_user.getFirstName()} </span>
    </div>
    <div class="field">
        <label>Login : </label><span> ${connected_user.getLogin()} </span>
    </div>
    <div class="field">
        <label>Profile : </label><span> ${connected_user.getRole()}
    </span>
    </div>
    <div class="fieldset_label">
        <span>Forums</span>
    </div>
    <div class="field">
        <a href="ViewForum">Forums</a>
    </div>
    <div class="field">
        <a href="following">My subscriptions</a>
    </div>

</div>

<%
    String role = (String) request.getSession().getAttribute("role");
    if (role.equals("Admin")) {
        out.println("<div class='fieldset'>");
        out.println("	<div class='fieldset_label'>");
        out.println("		<span>Section Admin</span>");
        out.println("	</div>");
        out.println("	<div class='field'>");

        out.println("      	<a href='forumManager.jsp'>Handle forums</a>");
        out.println("<br>");
        out.println("      	<a href='NouveauUtilisateur.html'>Create a New user</a>");
        out.println("	</div>");
        out.println("</div>");
    }
%>


</body>
</html>