<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="model.Forum"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>View Forum</title>
</head>
<body>

<%
    List<Forum> forumList = (List<Forum>) request.getSession().getAttribute("all_forums");
    List<Forum> forumListNotFollow = (List<Forum>) request.getSession().getAttribute("all_forumsNotfollow");
    if (forumList == null ) {
        out.println("null");
    }else{
        out.println("<div class = 'fieldset'>");
        out.println("	<div class = 'fieldset_label'>");
        out.println("		<span> Forum <span>");
        out.println("	</div>");
        out.println("<table>");
        out.println("<tr>");
        out.println("	<th> Title </th>");
        out.println("	<th> description </th>");
        out.println("	<th> Owner </th>");
        out.println("	<th> Follow </th>");
        out.println("</tr>");

        for (Forum forum : forumList) {

            out.println("<div class = 'field'>");
            out.println("<tr>");
            out.println("	<td>" + forum.getTitle() + "</td>");
            out.println("	<td>" + forum.getDescription() + "</td>");
            String owner = "";
            if (forum.getOwner() != null) {
                owner = forum.getOwner().getLastName();
            }
            out.println("	<td>" + owner + "</td>");
            out.println("	<td> <a href='messageforum?forumId=" + forum.get_id() + "'>see</a></td>");
            if (forumListNotFollow != null) {
                if ( forumListNotFollow.contains(forum)) {

                    out.println("<td> <a href='following?sub=" + forum.get_id() + "'>follow</a></td>");
                    out.println("</tr>");
                    out.println("</div>");

                } else {
                    out.println("<td>  already follow</a></td>");
                    out.println("</tr>");
                    out.println("</div>");


                }
            }else {//forum empty and they are all not  being followed by this user
                out.println("<td> <a href='following?sub=" + forum.get_id() + "'>follow</a></td>");
                out.println("</tr>");
                out.println("</div>");

            }
        }
        out.println("</table>");
        out.println(" <a href='Hub'>Home</a>");
        out.println("</div>");
    }
%>

</body>
</html>